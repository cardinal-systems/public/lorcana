import { useGetSets } from "@/actions/sets";
import GridCard from "@/components/GridCard";
import { notFound } from "next/navigation";

export default async function Sets({
  searchParams: { page = 1, pageSize = 12 },
}: {
  searchParams: {
    page: number;
    pageSize: number;
  };
}) {
  const sets = await useGetSets({ page, pageSize });

  return (
    <div className="px-6 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4">
      {sets.length > 0
        ? sets.map((set) => (
            <GridCard
              key={set.Set_Num}
              className={"h-30 flex flex-1"}
              image={`/images/sets/${set.Set_ID}.webp`}
              name={set.Set_ID}
              filter={`?filters=Set_ID=${set.Set_ID};`}
            />
          ))
        : notFound()}
    </div>
  );
}
