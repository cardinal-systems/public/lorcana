import { useGetCardCount, useGetCards } from "@/actions/cards";
import CardFilters from "@/components/CardFilters";
import GridCard from "@/components/GridCard";
import GridPagination from "@/components/GridPagination";
import { Spinner } from "@nextui-org/spinner";
import { notFound } from "next/navigation";
import { Suspense } from "react";

export default async function Cards({
  searchParams: { page = 1, pageSize = 12, filters },
}: {
  searchParams: {
    page: number;
    pageSize: number;
    filters?: string;
  };
}) {
  const totalCards = await useGetCardCount(filters);
  const cards = await useGetCards({
    page,
    pageSize,
    filters,
  });

  const totalPages = Math.ceil(totalCards / pageSize);

  return (
    <>
      <CardFilters page={page} pageSize={pageSize} filters={filters} />
      <Suspense
        key={`${page}-${pageSize}`}
        fallback={<Spinner color="primary" />}
      >
        <div className="px-6 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4">
          {cards.length > 0
            ? cards.map((card) => (
                <GridCard
                  key={card.Name}
                  image={card.Image}
                  name={card.Name}
                  filter={`/${card.Name}`}
                />
              ))
            : notFound()}
        </div>
      </Suspense>
      <GridPagination page={page} totalPages={totalPages} />
    </>
  );
}
