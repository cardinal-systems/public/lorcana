import { useGetCard } from "@/actions/cards";
import { Suspense } from "react";
import { Card, CardHeader, CardBody, CardFooter } from "@nextui-org/card";
import { Link } from "@nextui-org/link";
import { Image } from "@nextui-org/image";
import { Spinner } from "@nextui-org/spinner";

export default async function CardView({
  params: { name },
}: {
  params: {
    name: string;
  };
}) {
  const card = await useGetCard(decodeURIComponent(name));

  return (
    <Suspense fallback={<Spinner color="primary" />}>
      <Card className="p-6 mx-auto">
        <CardHeader>
          <h2 className="font-bold text-inherit">{card.Name}</h2>
        </CardHeader>
        <CardBody className="flex flex-row">
          <Image
            className="rounded-lg overflow-hidden justify-center aspect-16-9"
            alt={card.Name}
            src={card.Image}
            width={300}
          />
          <div className="flex flex-col ml-6">
            <div className="flex flex-row">
              Type:
              <p>{card.Type}</p>
            </div>
            {!!card.Body_Text && (
              <div className="flex flex-row">
                Description:
                <p>{card.Body_Text}</p>
              </div>
            )}
            {!!card.FlavourText && (
              <div className="flex flex-row">
                Flavour:
                <p>{card.Body_Text}</p>
              </div>
            )}
            <div className="flex flex-row">
              Set:
              <p>{card.Set_Name}</p>
            </div>
          </div>
        </CardBody>
        <CardFooter>
          <Link title="View Cards" href="/cards" />
        </CardFooter>
      </Card>
    </Suspense>
  );
}
