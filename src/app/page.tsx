"use client";

import { Button } from "@nextui-org/react";
import { useRouter } from "next/navigation";

export default function Home() {
  const { replace } = useRouter();

  return (
    <main className="flex flex-row items-center gap-6 p-24">
      <Button className="flex-1" onClick={() => replace("/cards")}>
        View Cards
      </Button>
      <Button className="flex-1" onClick={() => replace("/sets")}>
        View Sets
      </Button>
    </main>
  );
}
