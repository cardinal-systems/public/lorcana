import Header from "@/components/Header";
import ThemeProvider from "@/app/providers/ThemeProvider";
import UIProvider from "@/app/providers/UIProvider";
import type { Metadata } from "next";
import "@/styles/global.css";

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="light" suppressHydrationWarning>
      <body>
        <UIProvider>
          <ThemeProvider
            attribute="class"
            defaultTheme="system"
            enableSystem
            disableTransitionOnChange
          >
            <Header />
            {children}
          </ThemeProvider>
        </UIProvider>
      </body>
    </html>
  );
}

export const metadata: Metadata = {
  title: {
    template: "Lorcana - %s",
    default: "Lorcana",
  },
  icons: ["/images/logo.jpeg"],
  description: "Lorcana card manager using Next JS",
};
