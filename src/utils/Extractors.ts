import queryString from "query-string";

export type Filter = {
    [key: string]: string;
  };

export const ExtractFilters = (filters?: string) => {
    const filterObject: Filter = {};
  
    if (!filters) return filterObject as Filter;
  
    const filterPairs = filters.split(";");
  
    for (var filterPair of filterPairs) {
      const filterKeyValuePair = filterPair.split("=");
      if (filterKeyValuePair.length < 2) continue;

      filterObject[filterKeyValuePair[0]] = filterKeyValuePair[1];
    }
  
    return filterObject as Filter;
};
  
export const ConvertFiltersToQuery = (filters: Filter) => {
    let query = "";

    for (let [key, value] of Object.entries(filters)) {
      for (let queryValue of String(value ?? "").split(",")) {
        query = `${query}${key}=${queryValue};`;
      }
    }

    return encodeURIComponent(query);
};

export const GetUrlWithQuery = (currentUrl: string, additionalQueryProps: { [key: string] : string }) => {
  const url = new URL(currentUrl);
  const query = queryString.parse(url.search);

  for (var [key, value] of Object.entries(additionalQueryProps)) {
    if (key === "filters") {
      query[key] = String(query[key] ?? "").concat(value);
      continue;
    }

    query[key] = value;
  }

  url.search = queryString.stringify(query);

  return url.href;
}