"use client";

import { GetUrlWithQuery } from "@/utils/Extractors";
import { Pagination } from "@nextui-org/react";
import { useRouter } from "next/navigation";

export default function GridPagination({
  page = 1,
  totalPages = 1,
}: {
  page: number;
  totalPages: number;
}) {
  const { replace } = useRouter();
  return (
    <Pagination
      className="py-6"
      showControls
      initialPage={page}
      total={totalPages}
      onChange={(page) =>
        replace(GetUrlWithQuery(window.location.href, { page: String(page) }))
      }
    />
  );
}
