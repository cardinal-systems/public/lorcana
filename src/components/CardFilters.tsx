"use client";

import { CardColour, CardRarity, CardType } from "@/actions/cards";
import { useDebounce } from "@/hooks/useDebounce";
import {
  ExtractFilters,
  ConvertFiltersToQuery,
  Filter,
  GetUrlWithQuery,
} from "@/utils/Extractors";
import { Select, SelectItem } from "@nextui-org/select";
import { Slider } from "@nextui-org/slider";
import { Selection } from "@nextui-org/react";
import { useRouter } from "next/navigation";
import { useCallback, useEffect, useState } from "react";

export default function CardFilters({
  page,
  pageSize,
  filters,
}: {
  page: number;
  pageSize: number;
  filters?: string;
}) {
  const { replace } = useRouter();
  const [currentFilters, setCurrentFilters] = useState<Filter>(
    ExtractFilters(filters)
  );
  const debouncedValue = useDebounce(currentFilters, 500);

  const handleSelectFilterChange = useCallback(
    (argName: string, argValue: Selection) => {
      let filter = argValue as string;

      if (typeof argValue === "object") {
        filter = Array.from(argValue).join(",");
      }

      setCurrentFilters((prev) => ({
        ...prev,
        [argName]: filter,
      }));
    },
    [setCurrentFilters]
  );

  const renderField = useCallback(
    (key: string) =>
      currentFilters[key] ? String(currentFilters[key]).split(",") : [],
    [currentFilters]
  );

  useEffect(() => {
    replace(
      GetUrlWithQuery(window.location.href, {
        page: String(page),
        pageSize: String(pageSize),
        filters: ConvertFiltersToQuery(debouncedValue),
      })
    );
  }, [debouncedValue, page, pageSize, replace]);

  const costFilter = renderField("Cost");

  return (
    <nav className="px-6 pb-4">
      <div className="block flex-1 gap-2 md:flex">
        <Select
          className="pb-2 md:pb-0"
          radius="md"
          label="Colour"
          variant="bordered"
          selectionMode="multiple"
          selectedKeys={renderField("Color")}
          onSelectionChange={(keys: Selection) =>
            handleSelectFilterChange("Color", keys)
          }
        >
          {Object.values(CardColour).map((colour) => (
            <SelectItem key={colour} value={colour}>
              {colour}
            </SelectItem>
          ))}
        </Select>
        <Select
          className="pb-2 md:pb-0"
          variant="bordered"
          radius="md"
          label="Rarity"
          selectionMode="multiple"
          selectedKeys={renderField("Rarity")}
          onSelectionChange={(keys: Selection) =>
            handleSelectFilterChange("Rarity", keys)
          }
        >
          {Object.values(CardRarity).map((rarity) => (
            <SelectItem key={rarity} value={rarity}>
              {rarity}
            </SelectItem>
          ))}
        </Select>
        <Select
          className="pb-2 md:pb-0"
          variant="bordered"
          radius="md"
          label="Type"
          selectionMode="multiple"
          selectedKeys={renderField("Type")}
          onSelectionChange={(keys: Selection) =>
            handleSelectFilterChange("Type", keys)
          }
        >
          {Object.values(CardType).map((type) => (
            <SelectItem key={type} value={type}>
              {type}
            </SelectItem>
          ))}
        </Select>
      </div>
      <div className="block flex-1 gap-2 md:flex">
        <Slider
          label="Cost"
          step={1}
          minValue={1}
          maxValue={10}
          defaultValue={
            costFilter.length == 2
              ? [
                  !Number.isNaN(costFilter![0])
                    ? Number.parseInt(costFilter![0])
                    : 1,
                  !Number.isNaN(costFilter![1])
                    ? Number.parseInt(costFilter![1])
                    : 10,
                ]
              : [1, 10]
          }
          onChange={(value: number | number[]) =>
            setCurrentFilters((prev) => ({
              ...prev,
              Cost: (value as number[]).join(","),
            }))
          }
        />
      </div>
    </nav>
  );
}
