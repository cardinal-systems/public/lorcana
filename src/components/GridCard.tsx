import { Card, CardBody } from "@nextui-org/card";
import { Image } from "@nextui-org/image";
import { Link } from "@nextui-org/link";

export default function GridCard({
  className,
  image,
  name,
  filter,
}: {
  className?: string;
  image: string;
  name: string;
  filter?: string;
}) {
  return (
    <Link href={`/cards${filter}`} className={className}>
      <Card className={className}>
        <CardBody>
          <Image
            className={"rounded-lg aspect-16-9"}
            alt={name}
            src={image}
            height={300}
          />
        </CardBody>
      </Card>
    </Link>
  );
}
