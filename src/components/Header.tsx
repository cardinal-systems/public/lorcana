"use client";

import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
} from "@nextui-org/navbar";
import { Link } from "@nextui-org/link";
import { Image } from "@nextui-org/image";
import ThemeSelector from "@/components/ThemeSwitcher";

export default function Header() {
  return (
    <Navbar>
      <Link color="foreground" href="/">
        <NavbarBrand>
          <Image
            className="mr-4"
            alt={"lorcana-logo"}
            src="/images/logo.jpeg"
            height={30}
            width={30}
          />
          <p className="font-bold text-inherit">Lorcana</p>
        </NavbarBrand>
      </Link>
      <NavbarContent className="hidden sm:flex gap-4" justify="center">
        <NavbarItem>
          <Link color="foreground" href="/sets">
            Sets
          </Link>
        </NavbarItem>
        <NavbarItem>
          <Link color="foreground" href="/cards">
            Cards
          </Link>
        </NavbarItem>
      </NavbarContent>
      <NavbarContent className="hidden sm:flex gap-4" justify="end">
        <ThemeSelector />
      </NavbarContent>
    </Navbar>
  );
}
