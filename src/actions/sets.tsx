import { notFound } from "next/navigation";
import { QueryFilter } from "./cards";

type SetModel = {
  Set_ID: string;
  Set_Num: number;
  Release_Date: Date;
  Cards: Number;
  Name: string;
};

export const useGetSets = async ({
  page = 1,
  pageSize = 10,
  filters,
}: QueryFilter) => {
  const response = await fetch(
    `${
      process.env.LORCANA_API ?? ""
    }/sets/fetch?page=${page}&pageSize=${pageSize}${
      filters ? `&search=${filters}` : ""
    }`
  );

  try {
    return (await response.json()) as SetModel[];
  } catch {
    return notFound();
  }
};
