import { notFound } from "next/navigation";

export type QueryFilter = {
  page?: number;
  pageSize?: number;
  filters?: string;
};

export enum CardColour {
  STEEL = "Steel",
  RUBY = "Ruby",
  AMETHYST = "Amethyst",
  SAPHIRE = "Saphire",
  EMERALD = "Emerald",
  AMBER = "Amber",
}

export enum CardType {
  ACTION = "Action",
  CHARACTER = "Character",
  SONG = "Action - Song",
  LOCATION = "Location",
  ITEM = "Item",
}

export enum CardRarity {
  COMMON = "Common",
  UNCOMMON = "Uncommon",
  RARE = "Rare",
  SUPER_RARE = "Super Rare",
  LEGENDARY = "Legendary",
  ENCHANTED = "Enchanted",
}

export type CardModel = {
  Card_Num: number;
  Set_Num: number;
  Set_Name: string;
  Set_ID: string;
  Name: string;
  Color: CardColour;
  Cost: number;
  Inkable: boolean;
  Type: CardType;
  FlavourText: string;
  Body_Text: string;
  Image: string;
  Artist: string;
  Rarity: CardRarity;
};

export const useGetCardCount = async (filters?: string) => {
  const response = await fetch(
    `${process.env.LORCANA_API}/cards/fetch?${
      filters ? `&search=${filters}` : ""
    }`
  );

  try {
    let jsonResponse = (await response.json()) as CardModel[];
    return !!jsonResponse ? jsonResponse.length : 0;
  } catch {
    return notFound();
  }
};

export const useGetCards = async ({
  page = 1,
  pageSize = 10,
  filters,
}: QueryFilter) => {
  const response = await fetch(
    `${process.env.LORCANA_API}/cards/fetch?page=${page}&pageSize=${pageSize}${
      filters ? `&search=${filters}` : ""
    }`
  );

  try {
    let jsonResponse = (await response.json()) as CardModel[];
    return !!jsonResponse ? jsonResponse : notFound();
  } catch {
    return notFound();
  }
};

export const useGetCard = async (name: string) => {
  let response = await fetch(
    `${process.env.LORCANA_API}/cards/fetch?${encodeURIComponent(
      `search=Name=${name}`
    )}`
  );

  try {
    let jsonResponse = (await response.json())[0] as CardModel;
    if (!jsonResponse) return notFound();
    return jsonResponse;
  } catch {
    return notFound();
  }
};
